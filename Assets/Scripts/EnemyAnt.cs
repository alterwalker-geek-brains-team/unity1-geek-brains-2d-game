﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnt : MonoBehaviour
{

    public const int MOVING_RIGHT = 1;
    public const int MOVING_LEFT = -1;

    private const int IDLING_SPEED = 2;
    private const int ANGRY_SPEED = 5;
    private const int FOV = 90;// угол обзора
    private const float VIEW_DISTANCE  = 4f;

    private float _speed = IDLING_SPEED;
    private float _edgeCheckerShiftX = 0.5f;
    private float _edgeCheckerShiftY = -0.3f;
    private float _colliderFlipShift = -0.05f;
    private int _hp = 2;

    private SpriteRenderer _sr;
    private Rigidbody2D _rb;
    private CapsuleCollider2D _collider;
    private Transform _edgeChecker;
    private GameObject _player;
    private bool _edgeIsFarAway = true;
    private bool _isGrounded = true;
    private bool _isDead = false;
    

    private int _direction = MOVING_LEFT;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _sr = GetComponent<SpriteRenderer>();
        _player = GameObject.FindWithTag("Player");
        _collider = GetComponent<CapsuleCollider2D>();
        _edgeChecker = transform.Find("EdgeChecker");
    }

    // Update is called once per frame
    void Update()
    {
        checkForEdge();
    }

    private void FixedUpdate()
    {
        if (!_isDead) {
            Move();
        }

        SearchForPlayer();
    }

    private void Move()
    {
        _rb.velocity = new Vector2(_speed * _direction, _rb.velocity.y);
    }


    private void checkForEdge()
    {
        var edgeCheckerPosition = new Vector3(_edgeChecker.position.x + (_direction * _edgeCheckerShiftX), _edgeChecker.position.y + _edgeCheckerShiftY, _edgeChecker.position.z);
        _edgeIsFarAway = Physics2D.Linecast(transform.position, edgeCheckerPosition, 1 << LayerMask.NameToLayer("Ground"));
        _isGrounded = Physics2D.Linecast(transform.position, _edgeChecker.position, 1 << LayerMask.NameToLayer("Ground"));

        //Debug.DrawLine(transform.position, edgeCheckerPosition, Color.red);
        //Debug.DrawLine(transform.position, _edgeChecker.position, Color.blue);

        if (!_edgeIsFarAway && _isGrounded) {
            Flip();
        }
        
    }

    private void Flip()
    {
        _sr.flipX = !_sr.flipX;
        _direction = _direction * -1;

        var newOffsetX = _collider.offset.x + _direction * _colliderFlipShift;
        _collider.offset = new Vector2(newOffsetX, _collider.offset.y);
        
    }

    public void TakeDamage(int amount)
    {
        AudioManager.Play("EnemyOuch");

        _hp -= amount;

        if (_hp <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        _isDead = true;
        AudioManager.Play("EnemyDie");
        Destroy(GetComponent<Rigidbody2D>());
        GetComponent<Animator>().SetBool("isDead", true);
        Destroy(gameObject, 0.5f);
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Enemy")
        {
            Flip();
        }
    }

    private void SearchForPlayer()
    {

        
        var distance = Vector2.Distance(_player.transform.position, transform.position);


        if (distance > VIEW_DISTANCE)
        {
            SetIdle();
            return;
        }


        float angleToPlayer = getAngleToPlayer();

        //Debug.Log(angleToPlayer);
        //return;
        var fov = FOV / 2;

        if ( (angleToPlayer < (0 + fov) && angleToPlayer > (0 - fov) && _direction == MOVING_RIGHT) ||
             (angleToPlayer < (180 + fov) && angleToPlayer > (180 - fov) && _direction == MOVING_LEFT))
        {
            SetAngry();
        }
        else
        {
            SetIdle();
        }
    }

    private void SetAngry()
    {
        _speed = ANGRY_SPEED;

        //Debug.Log("ANGRY");
        //float angleToPlayer = getAngleToPlayer();

        //if ((angleToPlayer > 90 && _direction == MOVING_RIGHT) || (angleToPlayer < 90 && _direction == MOVING_LEFT))
        //{
        //    Flip();
        //}

    }

    private void SetIdle()
    {
        _speed = IDLING_SPEED;
    }

    private float getAngleToPlayer()
    {
        var TargetDirection = _player.transform.position - transform.position;

        float angleToPlayer = Vector2.Angle(transform.right, TargetDirection);

        return angleToPlayer;
    }
}
