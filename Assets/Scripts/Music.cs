﻿using UnityEngine;
using UnityEngine.UI;

public class Music : MonoBehaviour
{
    [SerializeField] Slider VolumeSlider;

    private AudioSource audioSource;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        audioSource = GetComponent<AudioSource>();
    }

    public void Volume()
    {
        audioSource.volume = VolumeSlider.value;
    }
}
