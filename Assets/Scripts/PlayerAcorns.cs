﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerAcorns : MonoBehaviour
{
    public Text textComponent;

    private int acornCount = 0;

    private void FixedUpdate()
    {
        textComponent.text = acornCount.ToString();
    }

    public void addAcorn(int amount)
    {
        acornCount += amount;
    }

    public int getAcornCount()
    {
        return acornCount;
    }

}
