﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcornPatron : MonoBehaviour
{
    [SerializeField] public ParticleSystem AcornSplinters;
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private SpriteRenderer capsuleCollider;
    private const float TIME_TO_DESTROY_GAME_OBJECT = 3;
    private const float TIME_TO_DESTROY_GAME_OBJECT_AFTER_COLLISION = 2;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        capsuleCollider = GetComponent<SpriteRenderer>();

    }

    private void Start()
    {
        Destroy(gameObject, TIME_TO_DESTROY_GAME_OBJECT);
    }

    public void Initialize(int _direction)
    {
        //_rb.velocity = new Vector2(_direction * 7, 3);
        rb.AddForce(new Vector2(_direction * 300, 150));
        rb.AddTorque(_direction * -200);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (collision.tag == "Enemy")
        {

            
            capsuleCollider.enabled = false;
            sr.enabled = false;
            rb.isKinematic = true;
            rb.velocity = new Vector2(0, 0);
            rb.angularVelocity = 0;
            gameObject.transform.rotation = Quaternion.identity;
            AcornSplinters.Play();
            collision.GetComponent<EnemyAnt>().TakeDamage(1);
            Destroy(gameObject, TIME_TO_DESTROY_GAME_OBJECT_AFTER_COLLISION);
        }
    }
}
