﻿using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    private const float SFX_PLAY_DELAY = 0.2f;

    static AudioSource AudioSrc;

    public static AudioClip[] PlayerJumpSounds;
    public static AudioClip[] PlayerOuchSounds;
    public static AudioClip[] EnemyOuchSounds;
    public static AudioClip[] EnemyDieSounds;
    public static AudioClip AcornPickupSound;
    public static AudioClip AcornThrowSound;
    public static AudioClip KeyPickupSound;

    [SerializeField] Slider VolumeSlider;

    private float timeToPlaSFX;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        timeToPlaSFX = Time.time;
    }


    void Start()
    {
        AudioSrc = GetComponent<AudioSource>();
        
        PlayerJumpSounds = Resources.LoadAll<AudioClip>("Audio/Player/Jumps/");
        PlayerOuchSounds = Resources.LoadAll<AudioClip>("Audio/Player/Ouch/");
        EnemyOuchSounds = Resources.LoadAll<AudioClip>("Audio/Enemy/Ouch/");
        EnemyDieSounds = Resources.LoadAll<AudioClip>("Audio/Enemy/Die/");
        AcornPickupSound = Resources.Load<AudioClip>("Audio/Misc/acorn-pickup");
        AcornThrowSound = Resources.Load<AudioClip>("Audio/Misc/acorn-throw");
        KeyPickupSound = Resources.Load<AudioClip>("Audio/Misc/key-pickup");

    }

    public void Volume()
    {
        AudioSrc.volume = VolumeSlider.value;
        if (Time.time > timeToPlaSFX) {

            PlaySound(PlayerOuchSounds);
            timeToPlaSFX = Time.time + SFX_PLAY_DELAY;
        }
        
    }


    public static void Play(string Sound) {

        switch (Sound) {
            case "PlayerJump":
                PlaySound(PlayerJumpSounds);
                break;

            case "PlayerOuch":
                PlaySound(PlayerOuchSounds);
                break;

            case "EnemyOuch":
                PlaySound(EnemyOuchSounds);
                break;

            case "EnemyDie":
                PlaySound(EnemyDieSounds);
                break;

            case "AcornPickup":
                PlaySound(AcornPickupSound);
                break;

            case "AcornThrow":
                PlaySound(AcornThrowSound);
                break;

            case "KeyPickup":
                PlaySound(KeyPickupSound);
                break;

            default:
                Debug.Log("Мы такую музыку не знаем.");
                break;
        }
    }

    private static void PlaySound(AudioClip[] clip)
    {
        int i = Random.Range(0, clip.Length);
        AudioSrc.PlayOneShot(clip[i]);
    }

    private static void PlaySound(AudioClip clip)
    {
        AudioSrc.PlayOneShot(clip);
    }

}
