﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    private float repeatDamagePeriod = 2f; // Как часто нам могут нанести урон (период бессмертия)
    private float hurtForce = 40;          // Как сильно нас отбросит, когда получим урон
    private int health = 5;
    private int maxHP = 5;
    private Player player;
    private Animator animator;
    private bool isImmortal = false;

    public Image[] hearts;
    public Sprite fullHeart;
    //public Sprite halfHeart;
    public Sprite emptyHeart;
    private float lastHitTime = 0f;

    public int getHealth() {
        return health;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        player = GetComponent<Player>();
    }

    private void Update()
    {
        DrawHearts();
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if (Time.time > lastHitTime + repeatDamagePeriod) {
                lastHitTime = Time.time;
                TakeDamage(collision.gameObject.transform);
            }
        }
    }

    public void setImmortal(bool value)
    {
        isImmortal = value;
    }

    public void TakeDamage(Transform whatUsDamages)
    {
        if (isImmortal) {
            return;
        }
            

        AudioManager.Play("PlayerOuch");

        health -= 1;

        if (health > 0)
        {
            // Create a vector that's from the enemy to the player with an upwards boost.
            Vector3 hurtVector = transform.position - whatUsDamages.position + Vector3.up * 5f;

            // С векторами надо разобраться, есть ощущение, что он неверно высчитывает направление
            //Debug.DrawRay(transform.position, hurtVector, Color.red);
            //Debug.DrawLine(transform.position, hurtVector,Color.blue);

            // Add a force to the player in the direction of the vector and multiply by the hurtForce.
            GetComponent<Rigidbody2D>().AddForce(hurtVector * hurtForce);

            StartCoroutine(DisableControlAndAnimateDamaging());
            // disableControlAndAnimateDamaging()
        }
        else
        {
            StartCoroutine(Die());
        }


    }

    IEnumerator DisableControlAndAnimateDamaging()
    {
        animator.SetTrigger("isHeart");
        player.disableControl();
        yield return new WaitForSeconds(0.8f);
        player.enableControl();
    }

    IEnumerator Die() {
        //Debug.Log("Die");
        // TODO: Реализовать смерть получше
        animator.SetTrigger("isHeart");
        player.disableControl();
        yield return new WaitForSeconds(0.2f);
        animator.SetBool("isDead", true);
        GetComponent<CapsuleCollider2D>().enabled = false;
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
        Application.LoadLevel(0);
    }
 

    private void DrawHearts()
    {
        if (health > maxHP)
        {
            health = maxHP;
        }

        for (int i = 0; i < hearts.Length; i++)
        {

            if (i < health )
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }


            if (i < maxHP )
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }
}

