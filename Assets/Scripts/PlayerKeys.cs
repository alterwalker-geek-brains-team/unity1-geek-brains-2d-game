﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerKeys : MonoBehaviour
{
    public Text textComponent;

    private int keyCount = 0;

    private void FixedUpdate()
    {
        textComponent.text = keyCount.ToString();
    }

    public void addKey(int amount)
    {
        keyCount += amount;
    }

    public int getKeyCount()
    {
        return keyCount;
    }
}
