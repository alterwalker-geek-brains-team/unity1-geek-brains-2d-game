﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchBug : MonoBehaviour
{

    [SerializeField] private GameObject _bug;

    private Player Player;                       // Подключаеся к скрипту игрока, чтобы получать его текущее направление
    private bool _isBugLaunnched = false;        // Выпущен ли сейчас жук
    private bool _launchBugNow = false;          // KeyDown плохо отрабатывает в fixUpdate, переносим в update

    // Start is called before the first frame update
    void Start()
    {
        Player = GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        shouldWeLaunchBug();
    }

    private void FixedUpdate()
    {

        if (_launchBugNow)
        {
            LaunchBugNow();
        }
    }

    private void LaunchBugNow()
    {

        var bug = Instantiate(_bug, transform.position, new Quaternion());
        bug.GetComponent<BombBug>().Initialize(Player.GetDirection());
  
        _isBugLaunnched = true;
        _launchBugNow = false;
    }

    private void shouldWeLaunchBug()
    {

        if (Input.GetButtonDown("Fire2") && Player.isControlEnabled())
        {
            _launchBugNow = true;
        }

    }
}
