﻿
using UnityEngine;

public class AntSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _ant;
    [SerializeField] int _spawnDelay = 5;
    [SerializeField] int _spawnAmount = 1;
   
    private bool _timeToSpawn = true;


    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Spawn", 0, _spawnDelay);

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        checkForLiveChilds();

       
    }

    private void Spawn()
    {
    if (_timeToSpawn)
    {
        var ant = Instantiate(_ant, transform.position, new Quaternion());
        ant.transform.parent = gameObject.transform;
        _timeToSpawn = false;
    }
    }

    private void checkForLiveChilds()
    {

        if (gameObject.transform.childCount < _spawnAmount)
        {
            _timeToSpawn = true;
        }
       
    }

}
