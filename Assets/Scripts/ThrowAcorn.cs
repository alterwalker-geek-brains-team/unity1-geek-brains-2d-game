﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowAcorn : MonoBehaviour
{

    [SerializeField] private GameObject _acorn;

    private Player Player;                        // Подключаеся к скрипту игрока, чтобы получать его текущее направление

    private bool _throwAcornNow = false;          // KeyDown плохо отрабатывает в fixUpdate, переносим в update

    // Start is called before the first frame update
    void Start()
    {
        Player = GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        shouldWeThrowAcorn();
    }

    private void FixedUpdate()
    {
        if (_throwAcornNow)
        {
            ThrowAcornNow();
        }
    }

    private void ThrowAcornNow()
    {
        var _direction = Player.GetDirection();

        AudioManager.Play("AcornThrow"); 

        var patron = Instantiate(_acorn, transform.position, new Quaternion());
        patron.GetComponent<AcornPatron>().Initialize(_direction);

        _throwAcornNow = false;
    }

    private void shouldWeThrowAcorn()
    {

        if (Input.GetButtonDown("Fire1") && Player.isControlEnabled())
        {
            _throwAcornNow = true;
        }

    }
}
