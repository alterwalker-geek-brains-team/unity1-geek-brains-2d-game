﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public const int MOVING_RIGHT = 1;
    public const int MOVING_LEFT = -1;

    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce = 380;
    
    private int _direction = MOVING_RIGHT;
    private bool _controlEnabled = true;
    private Rigidbody2D _rb;
    private SpriteRenderer _sr;
    private Animator _animator;
    private Transform _groundCheck;			// A position marking where to check if the player is grounded.
    private bool _grounded = false;			// Whether or not the player is grounded.
    private Camera _mc;
    private GameObject _bg;
    
    // Start is called before the first frame update
    void Start()
    {
        
        _groundCheck = transform.Find("groundCheck");
        _rb = GetComponent<Rigidbody2D>();
        _sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        _mc = Camera.main;
        _bg = GameObject.Find("Backgorund");
    }

    // Update is called once per frame
    void Update()
    {
        //_mc.transform.position = new Vector3(transform.position.x, transform.position.y, _mc.transform.position.z);
        //_bg.transform.position = new Vector3(transform.position.x, transform.position.y, _bg.transform.position.z);
        // TODO: Исправить работу с бэкгруандом, переделать на паралакс какой-нибудь
        _bg.transform.position = _mc.transform.position;
        checkForGrounded();

    }

    private void FixedUpdate()
    {
        if (_controlEnabled)
        {
            float hAxis = Input.GetAxis("Horizontal");
            float vAxis = Input.GetAxis("Vertical");

            if (hAxis > 0)
            {
                _sr.flipX = false;
                _direction = MOVING_RIGHT;
                Move();
            }
            else if (hAxis < 0)
            {
                _sr.flipX = true;
                _direction = MOVING_LEFT;
                Move();
            }
            else
            {
                Stop();
            }

            if (vAxis > 0)
            {
                Jump();

            }
        }  

    }

    public int GetDirection()
    {
        return _direction;
    }

    public void disableControl()
    {
        _controlEnabled = false;
    }

    public void enableControl()
    {
        _controlEnabled = true;
    }

    public bool isControlEnabled()
    {
        return _controlEnabled;
    }

    private void Move()
    {
        _animator.SetBool("isRunning", true);
        _rb.velocity = new Vector2(_speed * _direction, _rb.velocity.y);
    }

    private void Stop()
    {
        _animator.SetBool("isRunning", false);
        _rb.velocity = new Vector2(0, _rb.velocity.y);
    }

    private void Jump()
    {
        if (_grounded) {
            AudioManager.Play("PlayerJump");
            _animator.SetBool("isJumping", true);
            //_rb.velocity = new Vector2(_rb.velocity.x, _jumpHeight);
            _rb.AddForce(Vector2.up * _jumpForce);
            _grounded = false;
        }
    }



    private void checkForGrounded() {

        _grounded = Physics2D.Linecast(transform.position, _groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if (_grounded)
        {
            _animator.SetBool("isJumping", false);

        }

    }

}
