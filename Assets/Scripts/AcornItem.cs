﻿using UnityEngine;

public class AcornItem : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            collision.GetComponent<PlayerAcorns>().addAcorn(1);
            AudioManager.Play("AcornPickup");
            Destroy(gameObject);
        }
    }
}
