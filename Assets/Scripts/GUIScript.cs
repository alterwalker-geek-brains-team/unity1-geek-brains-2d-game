﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIScript : MonoBehaviour
{
    private int boxHeight = 100;
    private int boxWidhth = 150;
    private GameObject player;
    private bool isImmortal = false;


    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    void OnGUI()
    {


        GUI.BeginGroup(new Rect(Screen.width - boxWidhth, Screen.height - boxHeight, boxWidhth, boxHeight));
        GUI.Box(new Rect(0, 0, boxWidhth, boxHeight), "Отладка");

        var health = player.GetComponent<PlayerHealth>().getHealth();
        GUI.Label(new Rect(10, 25, 50, 20), "Жизни:" + health);
        var acorns = player.GetComponent<PlayerAcorns>().getAcornCount();
        GUI.Label(new Rect(10, 50, 75, 20), "Жёлуди:" + acorns);

        if (GUI.Toggle(new Rect(10, 75, 100, 20), isImmortal, "Неуязвимость") != isImmortal)
        {
            isImmortal = !isImmortal; // toggle manually

            player.GetComponent<PlayerHealth>().setImmortal(isImmortal);

          
        }

        GUI.EndGroup();

       
        
        
    }
}
