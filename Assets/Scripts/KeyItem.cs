﻿using UnityEngine;

public class KeyItem : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            collision.GetComponent<PlayerKeys>().addKey(1);
            AudioManager.Play("KeyPickup");
            Destroy(gameObject);
        }
    }
}
