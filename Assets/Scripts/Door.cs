﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    private const float SHOW_MESSAGE_SECONDS = 2f;

    [SerializeField] int KeysToOpen = 3;
    public Text textComponent;

    private void Start()
    {
        textComponent.text = "";
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            
            var keysToOpenLeft = KeysToOpen - collision.GetComponent<PlayerKeys>().getKeyCount();

            if (keysToOpenLeft > 0)
            {
                StartCoroutine(ShowMessage("Вам нужно собрать ещё ключей: " + keysToOpenLeft.ToString()));
            }
            else
            {
                SceneManager.LoadScene("Level02");
            }
            
        }
    }

    IEnumerator ShowMessage(string Message, float WaitTime = SHOW_MESSAGE_SECONDS)
    {
        textComponent.text = Message;

        yield return new WaitForSeconds(WaitTime);

        textComponent.text = "";

    }
}
