﻿using System.Collections;
using UnityEngine;

public class BombBug : MonoBehaviour
{

    private Rigidbody2D _rb;
    private SpriteRenderer _sr;
    private Animator _animator;
    private Transform _transform;
    private CircleCollider2D _collider;
    private Vector3 _shiftedCirclePosition;
    private int _speed = 2;
    private float _timeToExplode = 2f;
    private float _ScaleMultiplaer = 1.8f;
    private float _radius = 1.4f;
    private float _shiftCirclePositionX = 0f;
    private float _shiftCirclePositionY = 0.2f;


    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        _transform = GetComponent<Transform>();
        _collider = GetComponent<CircleCollider2D>();
    }

    public void Initialize(int direction)
    {
        _sr.flipX = direction > 0;
        _speed = _speed * direction;
        Invoke("Explode", _timeToExplode);
        _rb.velocity = new Vector2(_speed, _rb.velocity.y);
    }

    private void Explode()
    {
        _animator.SetBool("explode", true);
        _rb.velocity = new Vector2(0, 0);

        // Сначала получаем всех, кто оказался в радиусе взрыва
        CalculateShiftedPositon();
        var colliders = Physics2D.OverlapCircleAll(_shiftedCirclePosition, _radius, 1 << LayerMask.NameToLayer("Enemies"));


        // Потом отодивраем всех взрывом, увеличивая коллайдер
        _collider.isTrigger = false;
        _transform.localScale = new Vector3(_ScaleMultiplaer, _ScaleMultiplaer, 1.0f);

        //Затем спустя некотрое время раздаём дамагу
        StartCoroutine(giveDamageAndDestroy(colliders));

    }

    IEnumerator giveDamageAndDestroy(Collider2D[] colliders)
    {
        yield return new WaitForSeconds(0.2f);

        for (var i = 0; i < colliders.Length; i++)
        {
            colliders[i].GetComponent<EnemyAnt>().TakeDamage(3);
            //Debug.Log(colliders[i]);
        }

        Destroy(gameObject);

    }


    // Метод корректировки положения центра OverlapCircleAll
    private void CalculateShiftedPositon()
    {
         _shiftedCirclePosition = new Vector3(_transform.position.x + _shiftCirclePositionX, _transform.position.y + _shiftCirclePositionY, 0);
    }


    // Рисуем Гизмо, чтобы понимать кто должен попасть в радиус, а кто нет
    private void OnDrawGizmos()
    {
        CalculateShiftedPositon();
        Gizmos.DrawWireSphere(_shiftedCirclePosition, _radius);
        
    }


}
